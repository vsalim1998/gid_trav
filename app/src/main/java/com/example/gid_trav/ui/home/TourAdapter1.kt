package com.example.gid_trav.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.gid_trav.R

class TourAdapter1(private val sliderItems: MutableList<TourModel>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class SliderViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SliderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.slider_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return Int.MAX_VALUE
    }
}
package com.example.gid_trav.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.gid_trav.R

class TourAdapter(private val array: ArrayList<TourModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class TourViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TourViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.tour_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.findViewById<TextView>(R.id.title).text = array[position].title
        holder.itemView.findViewById<TextView>(R.id.location).text = array[position].location
    }

    override fun getItemCount(): Int {
        return array.size
    }

}

package com.example.gid_trav.ui.home

data class TourModel(val title: String, val location: String, val image: Int)
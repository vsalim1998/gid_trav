package com.example.gid_trav.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.example.gid_trav.R
import kotlin.math.abs


class HomeFragment : Fragment() {

    private val tourArray =
        arrayListOf(
            TourModel("Горная местность", "с. Хунзах, пр. Акушинского", R.drawable.mountains),
            TourModel("Горная местность", "с. Хунзах, пр. Акушинского", R.drawable.mountains),
            TourModel("Горная местность", "с. Хунзах, пр. Акушинского", R.drawable.mountains),
            TourModel("Горная местность", "с. Хунзах, пр. Акушинского", R.drawable.mountains),
            TourModel("Древняя культура", "с. Чох, окр. Закутли", R.drawable.mountains),
            TourModel("Древняя культура", "с. Чох, окр. Закутли", R.drawable.mountains),
            TourModel("Древняя культура", "с. Чох, окр. Закутли", R.drawable.mountains),
            TourModel("Древняя культура", "с. Чох, окр. Закутли", R.drawable.mountains),
            TourModel("Древняя культура", "с. Чох, окр. Закутли", R.drawable.mountains),
        )

    private lateinit var viewPager2: ViewPager2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = layoutInflater.inflate(R.layout.fragment_home, container, false)
        setupSpinnerAdapter(view)
        setupViewPager(view)
        setupRecyclerView(view)

        val cardView = view.findViewById<CardView>(R.id.cardView)
        cardView.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.nav_host_fragment_activity_main, MapFragment())?.commit()
        }
        return view
    }

    private fun setupRecyclerView(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = TourAdapter(array = tourArray)
        recyclerView.clipToPadding = false
        recyclerView.addItemDecoration(
            SpaceItemDecoration(
                space = 50,
                orientation = SpaceItemDecoration.Orientation.HORIZONTAL
            )
        )
    }

    private fun setupViewPager(view: View) {
        viewPager2 = view.findViewById(R.id.viewPager2)

        viewPager2.run {
            adapter = TourAdapter1(tourArray)
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 3
            setCurrentItem(20 * tourArray.size, false)
            getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        }

        val compositePageTransformer = CompositePageTransformer()
        compositePageTransformer.addTransformer { page, position ->
            val r = 1 - abs(position)
            page.scaleY = 0.85F + r * 0.15F
        }

        viewPager2.setPageTransformer(compositePageTransformer)

    //    viewPager2.setPageTransformer(MarginPageTransformer(6))

//        viewPager2.addItemDecoration(
//            SpaceItemDecoration(
//                space = 50,
//                orientation = SpaceItemDecoration.Orientation.HORIZONTAL
//            )
//        )
    }

    private fun setupSpinnerAdapter(view: View) {
        val spinnerCities = view.findViewById<Spinner>(R.id.spinner)
        val cityAdapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.cities,
            R.layout.spinner_layout_cities
        )
        spinnerCities.adapter = cityAdapter
    }
}